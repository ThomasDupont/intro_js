const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
if(process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}

const auth = require('./src/middlewares/auth.middleware');
const rateLimit = require('./src/middlewares/rateLimit.middleware');
const serialization = require('./src/middlewares/serialization.middleware');

const getTestAction = require('./src/actions/getTest.action');
const sumAction = require('./src/actions/sum.action');
const listOrgReposActions = require('./src/actions/github/listOrgRepos.action');
const getSingleRepoAction = require('./src/actions/github/getSingleRepo.action');
const updateSingleRepoAction = require('./src/actions/github/updateSingleRepo.action');

const app = express();

app.use(bodyParser.json());
app.use(cors());
app.get("/test", auth, rateLimit, getTestAction, serialization);
app.get('/sum', rateLimit, sumAction, serialization);

app.get('/orgs/:org/repos', listOrgReposActions, serialization);
app.get('/repos/:owner/:name', getSingleRepoAction, serialization);
app.patch('/repos/:owner/:repo', updateSingleRepoAction, serialization);

app.listen(process.env.PORT, 'localhost', () => console.log(`app started on ${process.env.PORT}`));