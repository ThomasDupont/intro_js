const github = require('../../services/github.service.js');

module.exports = async function updateSingleRepoAction(req, res, next) {
    res.rawResponse = await github.patch(`/repos/${req.params.owner}/${req.params.repo}`, {
        description: req.body.description
    });

    return next();
}