const github = require('../../services/github.service.js');

module.exports = async function getSingleRepoActions(req, res, next) {
    res.rawResponse = await github.get(`/repos/${req.params.owner}/${req.params.name}`);
    
    return next();
};