const github = require('../../services/github.service.js');

module.exports = async function listOrgReposAction(req, res, next) {
    res.rawResponse = await github.get(`/orgs/${req.params.org}/repos`);

    return next();
};
